import { combineReducers } from "redux";
import { numberReducer } from "./reducer/numberReducer";

export const rootReducer_DemoRedux = combineReducers({ numberReducer });
