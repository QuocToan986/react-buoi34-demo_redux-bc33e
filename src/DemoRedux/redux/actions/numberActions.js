import { GIAM_SO_LUONG, TANG_SO_LUONG } from "../constants/numberConstants";

export const tangSoLuong = (soLuong) => {
  return {
    type: TANG_SO_LUONG,
    payload: soLuong,
  };
};

export const giamSoLuong = (soLuong) => {
  return {
    type: GIAM_SO_LUONG,
    payload: soLuong,
  };
};
