import React, { Component } from "react";
import { connect } from "react-redux";
import { giamSoLuong, tangSoLuong } from "./redux/actions/numberActions";

class DemoRedux extends Component {
  render() {
    return (
      <div className="pt-4">
        <button
          onClick={() => this.props.tangSoLuong(1)}
          className="btn btn-success mr-3"
        >
          Tăng số lượng
        </button>
        <span className="display-4 text-danger">{this.props.number}</span>
        <button
          onClick={() => this.props.giamSoLuong(1)}
          className="btn btn-success ml-3"
        >
          Giảm số lượng
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    number: state.numberReducer.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    tangSoLuong: (soLuong) => {
      dispatch(tangSoLuong(soLuong));
    },
    giamSoLuong: (soLuong) => {
      dispatch(giamSoLuong(soLuong));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DemoRedux);
